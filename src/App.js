import React, { Component } from 'react';
import {Link, Route, Switch} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import GamesPage from './GamesPage'; 
import GamesForm from './GamesForm';

class App extends Component {
  render() {
    return (
      <div className="ui container">
        <div className="ui three item menu">
          <Link className="item" activeClassName="active" activeOnlyWhenExact to="">Home</Link>
          <Link className="item" activeClassName="active" activeOnlyWhenExact to="/games">Games</Link>
          <Link className="item" activeClassName="active" activeOnlyWhenExact to="/games/new">Add New Game</Link>
        </div>
        <Switch>
          <Route exact path="/games" component={GamesPage} />
          <Route exact path="/games/new" component={GamesForm} />  
          <Route exact path="game/:_id" component={GamesForm} />       
        </Switch>
      </div>
    );
  }
}

export default App;
